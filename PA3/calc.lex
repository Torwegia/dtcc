%{
	#include <stdio.h>
	#include "calc.tab.h"
%}

%%

\+			return PLUS;
\-			return MINUS;
\*			return MULT;
\/			return DIV;
[0-9]+		yylval = atoi(yytext); return INTEGER;
\;			return SEMI;
\(			return OPEN;
\)			return CLOSE;
[a-zA-Z.]*			return ERROR;

%%
