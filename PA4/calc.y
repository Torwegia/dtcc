%{
    #define YYSTYPE int

    #include <stdio.h>
    #include <stdlib.h>

    void yyerror(const char *str);

%}

%token PLUS MINUS MULT DIV INTEGER SEMI OPEN CLOSE ERROR

%left   PLUS
%left   MINUS
%left   MULT
%left   DIV

%%

start:
     | statement start   
     ;

statement: 
         | exp SEMI { printf("\t%d\n", $1); }
         ;

exp:
   | INTEGER                { $$ = $1;      }
   | exp PLUS exp       { $$ = $1 + $3; }
   | exp MINUS exp      { $$ = $1 - $3; }
   | exp MULT exp       { $$ = $1 * $3; }
   | exp DIV exp        { $$ = $1 / $3; }
   | OPEN exp CLOSE         { $$ = $2;      }
   ;

%%

void yyerror(const char *str)
{
    extern int yylineno;
    extern char *yytext;
    fprintf(stderr, "Error: %s at %s on line %d\n",str, yytext, yylineno);
}

int main()
{
    yyparse();
    return 0;
}
