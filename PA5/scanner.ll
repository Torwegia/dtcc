%{

#include <string>
#include "scanner.h"
#include "symbolTable.h"
#include "error.hpp"

typedef dtcc::Parser::token token;
typedef dtcc::Parser::token_type token_type;

#define yyterminate() return token::END

#define YY_NO_UNISTD_H

token_type check_type(char *);
void yyerror(const char *);
unsigned long int convertInt(char *val,int base, const dtcc::Parser::location_type& l );
unsigned long int convertBin(char *val,int len, const dtcc::Parser::location_type& l);
float convertFloat(char *val, const dtcc::Parser::location_type& l);

%}

%option c++

%option batch

%option prefix="DTCC"

%option debug

/*Line numbers?*/
%option yylineno

/*remove if we decide to handle includes*/
%option yywrap nounput

%option stack

%x comment

/*Track locations accurately*/
%{
#define YY_USER_ACTION yylloc->columns(yyleng);
%}

D       [0-9]
NZD	[1-9]
O	[0-7]
L       [a-zA-Z_]
H       [a-fA-F0-9]
BP	(0b|0B)
BD	(0|1)
HP	(0x|0X)
E	[Ee][+-]?{D}+
FS	(f|F|l|L)
IS 	({US}|{US}{LS}|{US}{LLS}|{LS}|{LS}{US}|{LLS}|{LLS}{US})
US	(u|U)
LS	(l|L)
LLS	(ll|LL)
BE	[Pp][+-]?{D}+
HQ	{H}{H}{H}{H}
UC	(\\u{HQ}|\\U{HQ})
EC	(a|b|f|n|r|t|v|\\|\?|\'|\")
OES	(\\{O}|\\{O}{O}|\\{O}{O}{O})	
HES	(\\x{H}+)
ES	(\\{EC}|{OES}|{HES}|{UC})
blank	[ \t]

		



%%

%{
	yylloc->step();
%}

"!!S"					{	 dtcc::SymbolTable::getInstance()->dumpState();}

{blank}+				{	 yylloc->step();			}

[\n]+					{	 yylloc->lines(yyleng); yylloc->step();	}
"//".*					{ 						}
"/*"					{	 BEGIN(comment);			}	
"case"					{	 return token::CASE;			}
"default"				{	 return token::DEFAULT;			}
"if"					{	 return token::IF;			}
"else"					{	 return token::ELSE;			}
"switch"				{	 return token::SWITCH;			}
"while"					{	 return token::WHILE;			}
"do"					{	 return	token::DO;			}
"for"					{	 return token::FOR;			}
"goto"					{	 return token::GOTO;			}
"continue"				{	 return token::CONTINUE;		}
"break"					{	 return token::BREAK;			}
"return"				{	 return	token::RETURN;			}

"typedef"				{	 return token::TYPEDEF;			}
"extern"				{	 return token::EXTERN;			}
"static"				{	 return token::STATIC;			}
"auto"					{	 return token::AUTO;			}
"register"				{	 return token::REGISTER;		}

"char"					{	 return token::CHAR;			}
"short"					{	 return token::SHORT;			}
"int"					{	 return token::INT;			}
"long"					{	 return token::LONG;			}
"signed"				{	 return token::SIGNED;			}
"unsigned"				{	 return token::UNSIGNED;		}
"float"					{	 return token::FLOAT;			}
"double"				{	 return token::DOUBLE;			}
"const"					{	 return	token::CONST;			}
"volatile"				{	 return	token::VOLATILE;		}
"void"					{	 return token::VOID;			}

"struct"				{	 return token::STRUCT;			}
"union"					{	 return token::UNION;			}
"enum"					{	 return	token::ENUM;			}
"..."					{	 return token::ELIPSIS;			}


{L}({L}|{D})*				{	 yylval->stringVal=yytext; return check_type(yytext);}
{NZD}{D}*{IS}?				{	 yylval->intVal=convertInt(yytext,10,*yylloc);
							return token::INTEGER_CONSTANT;	}
"0"*{O}+{IS}?				{	 yylval->intVal=convertInt(yytext,8,*yylloc);
							return token::INTEGER_CONSTANT; }
"0o"{O}+{IS}?				{	 yylval->intVal=convertInt(yytext,8,*yylloc);
							return token::INTEGER_CONSTANT;	}
{HP}{H}+{IS}?				{	 yylval->intVal=convertInt(yytext,16,*yylloc);
							return token::INTEGER_CONSTANT;	}
{BP}{BD}+{IS}?				{	 yylval->intVal=convertBin(yytext,yyleng,*yylloc);	
							return token::INTEGER_CONSTANT;	}

{D}+{E}{FS}?				{	 yylval->floatVal=convertFloat(yytext,*yylloc);
							return token::FLOATING_CONSTANT;	}
{D}*"."{D}+({E})?{FS}?			{	 yylval->floatVal=convertFloat(yytext,*yylloc); 
							return token::FLOATING_CONSTANT;	}
{D}+"."{D}*({E})?{FS}?			{	 yylval->floatVal=convertFloat(yytext,*yylloc); 
							return token::FLOATING_CONSTANT;	}
{HP}{H}*"."{H}+{BE}{FS}?		{	 yylval->floatVal=convertFloat(yytext,*yylloc); 
							return token::FLOATING_CONSTANT;	}
{HP}{H}+"."{H}*{BE}{FS}?		{	 yylval->floatVal=convertFloat(yytext,*yylloc); 
							return token::FLOATING_CONSTANT;	}
{HP}{H}+{BE}{FS}?			{	 yylval->floatVal=convertFloat(yytext,*yylloc); 
							return token::FLOATING_CONSTANT;	}

L?'[^'"\n]'				{	 return token::CHARACTER_CONSTANT; 	}
L?'{ES}'				{	 return token::CHARACTER_CONSTANT;	}
L?\"(\\.|[^\\"])*\"			{	 return token::STRING_LITERAL;		}

">>="					{	 return token::RIGHT_ASSIGN;		}
"<<="					{	 return token::LEFT_ASSIGN;		}
"*="					{	 return token::MUL_ASSIGN;		}
"/="					{	 return token::DIV_ASSIGN;		}
"+="					{	 return token::ADD_ASSIGN;		}
"-="					{	 return token::SUB_ASSIGN;		}	
"%="					{	 return token::MOD_ASSIGN;		}
"&="					{	 return token::AND_ASSIGN;		}
"^="					{	 return token::XOR_ASSIGN;		}
"|="					{	 return token::OR_ASSIGN;		}

">>"					{	 return token::RIGHT_OP;		}
"<<"					{	 return token::LEFT_OP;			}
"++"					{	 return token::INC_OP;			}
"--"					{	 return token::DEC_OP;			}
"->"					{	 return token::PTR_OP;			}
"&&"					{	 return token::AND_OP;			}
"||"					{	 return token::OR_OP;			}
"<="					{	 return token::LE_OP;			}
">="					{	 return token::GE_OP;			}
"=="					{	 return token::EQ_OP;			}
"!="					{	 return token::NE_OP;			}

("{"|"<%")				{	 return token::OPEN_CURLY;		}
("}"|"%>")				{	 return token::CLOSE_CURLY;		}
"("					{	 return token::OPEN_PAREN;		}
")"					{	 return token::CLOSE_PAREN;		}
("["|"<:")				{	 return token::OPEN_SQUARE;		}
("]"|":>")				{	 return token::CLOSE_SQUARE;		}
":"					{	 return token::COLON;			}
";"					{	 return token::SEMI;			}
"="					{	 return token::EQUAL;			}
"&"                    			{        return token::AND; 			}
"|"                    			{        return token::OR; 			}
"!"					{	 return token::NOT;			}
"~"					{	 return token::TILDE;			}
"+"					{	 return token::PLUS;			}
"-"					{	 return token::MINUS;			}
"/"					{	 return token::DIV;			}
"*"					{	 return token::MUL;			}
"<"					{	 return token::LE;			}
">"					{	 return token::GE;			}
"%"					{	 return token::MOD;			}
"^"					{	 return token::XOR;			}
"?"					{	 return token::Q_MARK;			}
"."					{	 return token::DOT;			}
","					{	 return token::COMMA;			}
.					{	 dtcc::ErrorLogger::error(*yylloc,"No matching scanner rule");}
<comment>[^*\n]*        /* eat anything that's not a '*' */
<comment>"*"+[^*/\n]*   /* eat up '*'s not followed by '/'s */
<comment>\n             {yylloc->lines(yyleng); yylloc->step();}
<comment>"*"+"/"        BEGIN(INITIAL);

%%

namespace dtcc 
{

	Scanner::Scanner(std::istream *in, std::ostream *out)
	:DTCCFlexLexer(in, out)
	{}

	Scanner::~Scanner()
	{}

	void Scanner::set_debug(bool b) 
	{
		yy_flex_debug = b;
	}

}
#ifdef yylex
#undef yylex
#endif

int DTCCFlexLexer::yylex()
{
	std::cerr << "[F] using the wrong yylex, something is amiss!" << std::endl;
	return 0;
}

int DTCCFlexLexer::yywrap()
{
	return 1;
}

/*TEMPORARY*/
void processComment()
{
	
	return;
}

token_type check_type(char *id)
{

	bool isTD=false;
	dtcc::Type* givenType;
	givenType=dtcc::SymbolTable::getInstance()->findType(id);
	if(givenType==NULL)
	{
		return token::IDENTIFIER;
	}
	else
	{
		isTD=givenType->isTypedef();
		if(isTD==true)
		{
			return token::TYPEDEF_NAME;	
		}
		else
		{
			return token::IDENTIFIER;
		}
	}
}
void yyerror(const char *msg)
{
	std::cerr<<"Error: "<<msg<<std::endl;
	exit(1);
}
unsigned long int convertInt(char *val, int base, const dtcc::Parser::location_type& yylloc)
{
	
	unsigned long int intValue=strtoul(val,NULL,base);
	unsigned long long int intValue2=strtoul(val,NULL,base);
	if(intValue2>4294967295)
	{
		dtcc::ErrorLogger::error(yylloc,"integer overflow");
	}
	return intValue;	
}
unsigned long int convertBin(char *val,int len,const dtcc::Parser::location_type& yylloc)
{
	int i=2,j=0,k=0,lenArr=0,tempVal=0;
	int intArray[len];
	unsigned long int finalVal=0;
	unsigned long long int finalVal2=0;

	
	for(i=2,j=0;i<len;i++)
	{	
		tempVal=val[i]-'0';
		if(tempVal==0 || tempVal==1)
		{
			intArray[j]=tempVal;
			j++;
		}
	}
	for(k=0,lenArr=j-1; lenArr>=0; lenArr--,k++)
	{
		finalVal=finalVal+intArray[k]*pow(2,lenArr);
		finalVal2=finalVal2+intArray[k]*pow(2,lenArr);
	
	}	

	 if(finalVal2>4294967295)
       	 {
                dtcc::ErrorLogger::error(yylloc,"integer overflow");
       	 }

	
	return finalVal;
	
}
float convertFloat(char *msg, const dtcc::Parser::location_type& yylloc)
{
	float x;
	bool infi=false;
	x=strtof(msg,NULL);
	infi=isinf(x);
	if(infi==true)
	{
		dtcc::ErrorLogger::error(yylloc,"float overflow");
	}
	return x;

}
