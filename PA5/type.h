#pragma once

#include <string>
#include <map>
#include <vector>

namespace dtcc {

//the mother of all types
//in the end types should only hold information about the type
//not instanciations(sp?) of the type
class Type
{
	public:
		virtual ~Type();

		enum TypeClass {
			Builtin,
			Pointer,
			Typedef,
			Function,
			Record,//structs unions
			Array,
			Enum
		};
		
		TypeClass getTC() const { return TC; }
		bool isConst() const;
		bool isExtern() const;
		virtual unsigned int getSize() const = 0;
		std::string toStr() const;
		static std::string TCToStr(TypeClass);

		virtual bool isBuiltin();
		virtual bool isPointer();
		virtual bool isTypedef();
		virtual bool isFunction();
		virtual bool isRecord();
		virtual bool isArray();
		virtual bool isEnum();


	protected:
		Type(TypeClass, bool c, bool e);

		//typeclass bitfield, describes the subclass the type belongs to
		TypeClass TC;

		//TODO: MOVE THESE TWO FLAGS TO SYMBOL
		//const?
		bool CONST;

		//extern? might not need for requirements
		bool EXTERN;
};

//All the built in types
class BuiltinType : public Type
{
	public:
		enum Kind {
			#include "builtinKinds.def"
		};

		BuiltinType( Kind k, unsigned int s, bool c, bool e );//const extern

		Kind getKind() const;
		unsigned int getSize() const;

		static std::string kindToStr( Kind );

		bool isBuiltin();

	private:
		Kind kind;
		unsigned int size;

};

//Pointers to other types
class PointerType : public Type
{
	public:
		PointerType( Type *t, bool c, bool e );

		Type* getType() const;

		unsigned int getSize() const;
	
		bool isPointer();

	private:
		Type* type;

};

//Typedefs, renames of other types
class TypedefType : public Type
{
	public:
		TypedefType( Type* t, bool c, bool e );

		Type* desugar() const;

		unsigned int getSize() const;

		bool isTypedef();

	private:
		Type* base;
};

//Functions, needed for function pointers
class FunctionType : public Type
{
	public:
		FunctionType( Type *r, bool c, bool e );//r points to return type, NULL == void

		Type* getReturnType() const;

		void addArg( Type *f );
		unsigned int getArgCount() const;
		Type* getArgType(unsigned int n) const;

		unsigned int getSize() const;

		bool isFunction();

	private:
		Type* ret;
		std::vector< Type* > args;

};

//Records, stucts and unions
class RecordType : public Type
{
	public:
		enum Kind {
			Union,
			Struct
		};

		RecordType( Kind k, std::vector<std::string> names, std::vector<Type*> types
				  , bool c, bool e );

		Kind getKind() const;
		unsigned int getSize() const;
		bool fieldExists( const std::string& f ) const;
		Type* getFieldType( const std::string& f );

		bool isRecord();

		std::vector<std::string> fields;//need to preserve order
		std::map<std::string, Type*> fieldTypes;
	private:
		Kind kind;
		unsigned int size;
};


//Arrays, collection of a single type
class ArrayType : public Type
{
	public:
		ArrayType( Type* t, unsigned int n, bool c, bool e );

		Type* getType() const;
		unsigned int getCapacity() const;

		unsigned int getSize() const;

		bool isArray();

	private:
		Type *type;

		unsigned int capacity;
};

//Enums, enumerated types
class EnumType : public Type
{
	public:
		EnumType( bool e );

		int getCount();
		void setCount( int newCount );
		void incrCount();
	
		bool isEnum();

	private:
		int count;
};

}
