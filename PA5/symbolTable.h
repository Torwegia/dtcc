#pragma once

#include "type.h"
#include "symbol.h"

#include <map>
#include <stack>
#include <string>
#include <vector>

namespace dtcc {

class SymbolTable {
	public:
		static SymbolTable* getInstance()
		{
			if( m_instance == NULL )
			{
				m_instance = new SymbolTable;
			}
			return m_instance;
		}

		void set_debug(bool b); 
		void dumpState();

		void pushScope();
		void popScope();

		Type* findType(const std::string&);
		bool addType(const std::string&, Type*);

		Symbol* findSymbol(const std::string&);
		bool addSymbol(const std::string&, Symbol*);//TODO: ADD ACCESS MODIFIERS

	private:
		bool debug;	
	
		SymbolTable();
		~SymbolTable();

		void initializeTypes();
		void initializeSymbols();

		void indent(int);

		typedef std::map< std::string, std::stack< Type* >> tTable;
		typedef std::map< std::string, std::stack< Symbol* >> sTable;

		unsigned int scopeLevel;

		static SymbolTable* m_instance;

		std::vector< tTable > types;
		std::vector< sTable > symbols;
};

}
