#!/bin/bash
if [ ! -e ./dtcc_test ]
then
	make
fi

for file in ./testCases/*
do
	echo $file
	./dtcc_test $file
	read -p "Press enter to continue..."
done
