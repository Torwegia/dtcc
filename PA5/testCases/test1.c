int g_val;
int mod_value (int value1, int value2);
void foo ();

// main
int main ()
{
	// Variable delcarations
	int a = 123;
	int b = 10;
	int c = 0;
	char array[15];
	int i;
	char *string = "lalala";
	float f = 1234.235;
    
	array[0] = 't';
	f = -122.1e-1;
	f = 122.1e+1;

	c = mod_value (a, b);

	// Determine the correct value for c
	if ((c < a) && (c < b))
	{
		c = a;
	}
	else
	{
		c = b;
	}


	// Set C appropriately
	for (i = 0; i < c; ++i)
	{
		a += b * i;

		if (a <= 0 || a >= 1000 || a != 5)
		{
			break;
		}
		else
		{
			continue;
		}
	}

	g_val = c;

	g_val = g_val + 0x00F + 0X124F;
	//g_val = g_val + 0o312 + 0O21;

HI:

	// switch
	switch (g_val)
	{
		case 0:
		{
			foo ();
		}
		break;

		default:
		{
			do
			{
				g_val--;
			} while (g_val);

			goto HI;	
		}
		break;
	}

	return 0;
}

// return the mod of two values
int mod_value (int value1, int value2)
{
	return (value1 % value2);
}

void foo ()
{
	g_val++;
	return;
}
