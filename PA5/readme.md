CS 660: Compiler Construction
Spring 2013
=============================

The Devyani Tor C Compiler(dtcc)
--------------------------------

A compiler for a subset of the C language.

Implementation
--------------
* Scanner
* Parser
* Symbol Table

Requirements
------------
dtcc requires the following libraries and tools

* libboost_program_options
* flex
* bison


Directions to run program
-------------------------
1. make
2. ./dtcc_test -d <debugLevel> <inputFile>

   debugLevels:
   	     0: none
	     1: Scanner
	     2: Parser

3. ./dtcc_test --help
 
Assumptions
-----------

+ You have -Werror on, kinda. Basically the compiler does not warn, it only stops
+ Unsized arrays are assumed to have size of 1
