#pragma once
#include <string>
#include <vector>

namespace dtcc {

class Driver {
	public:
		Driver(const std::string& fname);
		Driver(int dLevel, const std::string& fname);
		Driver(int dLevel, const std::string& fname
		      , const std::string& oname);

		//TODO: CHANGE/ALTER DEBUG LEVELS
		//the debug level of the whole thing
		// 0 - none
		// 1 - lexer
		// 2 - parser
		// 3 - symbol table
		// anything else - everything
		int debug_level;

		//input
		std::string filename;

		//filename for error messages
		std::string output;
		bool use_out;


		bool parse();

		void error(const class location& l, const std::string& m);

		void error(const std::string& m);


		//symbol table and other things below

		class Scanner* lexer;
};

}
