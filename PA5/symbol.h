#pragma once

#include <string>

#include "type.h"
#include "parser.h"

namespace dtcc {

class Symbol
{
	public:
		Symbol( const Parser::location_type& l, Type* t, const std::string& tn, bool c = false );

		Parser::location_type getLocation() const;
		Type* getType() const;
		std::string getTypeName() const;

		bool isIncomplete() const;
		void setComplete();

	private:
		Parser::location_type loc;
		Type *type;
		std::string typeName;
		bool incomplete;
};

}
