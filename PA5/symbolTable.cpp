#include "symbolTable.h"
#include <iostream>
#include <iomanip>
namespace dtcc {

typedef std::map< std::string, std::stack< Type* >> tTable;
typedef std::map< std::string, std::stack< Symbol* >> sTable;

//static variables
SymbolTable* SymbolTable::m_instance = NULL;

SymbolTable::SymbolTable()
:scopeLevel(0)
{
	//initialize type table
	initializeTypes();
	initializeSymbols();
}

void SymbolTable::initializeTypes() {
	tTable globalTypes;

	//generate all the builtin types
	globalTypes["UChar"].push(new BuiltinType( BuiltinType::UChar, 1, false, false ));
	globalTypes["UShort"].push(new BuiltinType( BuiltinType::UShort, 2, false, false ));
	globalTypes["UInt"].push(new BuiltinType( BuiltinType::UInt, 4, false, false ));
	globalTypes["ULong"].push(new BuiltinType( BuiltinType::ULong, 4, false, false ));
	globalTypes["ULongLong"].push(new BuiltinType( BuiltinType::ULongLong, 8, false, false ));
	globalTypes["Char"].push(new BuiltinType( BuiltinType::Char, 1, false, false ));
	globalTypes["Short"].push(new BuiltinType( BuiltinType::Short, 2, false, false ));
	globalTypes["Int"].push(new BuiltinType( BuiltinType::Int, 4, false, false ));
	globalTypes["Long"].push(new BuiltinType( BuiltinType::Long, 4, false, false ));
	globalTypes["LongLong"].push(new BuiltinType( BuiltinType::LongLong, 8, false, false ));
	globalTypes["Float"].push(new BuiltinType( BuiltinType::Float, 4, false, false ));
	globalTypes["Double"].push(new BuiltinType( BuiltinType::Double, 8, false, false ));
	globalTypes["Void"].push(new BuiltinType( BuiltinType::Void, 0, false, false ));
	

	types.push_back(globalTypes);
}

void SymbolTable::set_debug(bool b){

	debug=b;
}
void SymbolTable::initializeSymbols() {
	sTable globalSymbols;

	//TODO: DEFINE SOME DEFAULT SYMBOLS
	symbols.push_back(globalSymbols);
}


void SymbolTable::pushScope() {
	
	types.push_back(tTable());
	symbols.push_back(sTable());
	scopeLevel++;
	if(debug==true)
	{
		std::cout<<"Pushed scope level: "<<scopeLevel<<std::endl;
	}
}

void SymbolTable::popScope() {
	if(debug==true)	
	{
		std::cout<<"Popped scope level: "<<scopeLevel<<std::endl;
	}
	types.pop_back();
	symbols.pop_back();
	scopeLevel--;
}


Type* SymbolTable::findType( const std::string& k ) {
	Type* ret = NULL;
	if( types[scopeLevel].count(k) ) {
		ret = ( types[scopeLevel] )[k].top();//kinda gross
	} else if( types[0].count(k) ) {
		ret = ( types[0] )[k].top();//kinda gross
	}
	if(debug==true)
	{
		if(ret==NULL)
		{
			std::cout<<"Symbol: "<<k<<"\tType: Not found "<<std::endl;
		}
		else
		{
			std::cout<<"Symbol: "<<k<<"\tType: "<<ret->toStr()<<std::endl;
		}
	}
	return ret;
}

Symbol* SymbolTable::findSymbol( const std::string& k ) {
	Symbol* ret = NULL;
	if( symbols[scopeLevel].count(k) )
        {
		ret = ( symbols[scopeLevel] )[k].top();//kinda gross
		if(debug==true)
		{
			std::cout<<"Symbol: "<<k<<"\tFound at scope level: "<<scopeLevel<<std::endl;			
		}
	} 
	else if( symbols[0].count(k) ) 
	{
		ret = ( symbols[0] )[k].top();//kinda gross
		if(debug==true)
		{
			std::cout<<"Symbol: "<<k<<"\tFound in scope level: 0"<<std::endl;
		}
	}
	else
	{
		if(debug==true)
		{
			std::cout<<"Symbol: "<<k<<"\t Not found in table"<<std::endl;
		}
	}
	
	return ret;
}


bool SymbolTable::addType( const std::string& k, Type *t ) {
	if( types[scopeLevel].count(k) || types[0].count(k) ||
		symbols[scopeLevel].count(k) || symbols[0].count(k) )
	 {
		if(debug==true)
		{
			std::cout<<"WARNING: Tried to add type for "<<k<<". Type already exists."<<std::endl;
		}
		return false;//error
         } 
	else 
	{
		( types[scopeLevel] )[k].push(t);
		if(debug==true)
		{
			std::cout<<"Symbol: "<<k<<"\tAdded type: "<<t<<std::endl;
		}
		return true;
	}
}

bool SymbolTable::addSymbol( const std::string& k, Symbol *t ) {
	if( types[scopeLevel].count(k) || types[0].count(k) ||
		symbols[scopeLevel].count(k) || symbols[0].count(k) ) {
		//its a shadowing entry
		( symbols[scopeLevel] )[k].push(t);
		if(debug==true)
		{
			std::cout<<"WARNING: Symbol "<<k<<" is shadowing entry"<<std::endl;
		}
		return false;
		
	} else {
		( symbols[scopeLevel] )[k].push(t);
		if(debug==true)
		{
			std::cout<<"Symbol: "<<k<<"\tAdded to the scopelevel: "<<scopeLevel<<std::endl;
		}
		return true;
	}
	
}

void SymbolTable::dumpState() {
	//variable
	int i=0;
	int sizeMaxTypeOne=0;


	std::cout << "Type Table" << std::endl;
	std::cout << "==========" << std::endl;

	for(i=0; i<=scopeLevel;i++)
	{
		for(auto it:types[i])
		{
			if((it.first).size() > sizeMaxTypeOne)
			{
				sizeMaxTypeOne=(it.first).size();
			}
			
		}
	}
	//add two spaces to sizeMaxType
	sizeMaxTypeOne +=2;

	for(i = 0; i <= scopeLevel; i++)
	 {
		indent(i+i);
		std::cout << "Scope #" << i << ":" << std::endl;
		indent(i+i);
		std::cout << "---------" << std::endl;
		if( types[i].size() == 0 ) {
			indent(i+i);
			std::cout << "Empty" << std::endl;
		} else {
			for(auto it: types[i]) {	
				indent(i+i);
				std::cout<<it.first;
				indent(sizeMaxTypeOne-((it.first).size()));		
				std::cout<<it.second.top()->toStr()<<std::endl;	
			}
		}
	}
	
	sizeMaxTypeOne=0;
	for(i=0; i<=scopeLevel;i++)
	{
		for(auto it:symbols[i])
		{
			if((it.first).size() > sizeMaxTypeOne)
			{
				sizeMaxTypeOne=(it.first).size();
			}
			
		}
	}
	sizeMaxTypeOne +=2;
	
	std::cout << std::endl << std::endl;
	std::cout << "Symbol Table" << std::endl;
	std::cout << "============" << std::endl;
	for( int i = 0; i <= scopeLevel; i++) {
		indent(i+i);
		std::cout << "Scope #" << i << ":" << std::endl;
		indent(i+i);
		std::cout << "---------" << std::endl;
		if( symbols[i].size() == 0 ) {
			indent(i+i);
			std::cout << "Empty" << std::endl;
		} else {
			for(auto it: symbols[i]) {	
				indent(i+i);
				std::cout<<it.first;		
				indent(sizeMaxTypeOne-((it.first).size()));		
				std::cout<<it.second.top()->getTypeName();
				std::cout<<"@"<<it.second.top()->getLocation()<<std::endl;	
			}
		}
	}
	std::cout << std::endl << std::endl;
}

void SymbolTable::indent(int l) {
	while( l > 0) {
		std::cout << " ";
		l--;
	}
}

}//namespace
