#include "type.h"

namespace dtcc {

Type::Type(Type::TypeClass tc, bool c, bool e) {
	TC = tc;
	CONST = c;
	EXTERN = e;
}

Type::~Type()
{}

bool Type::isConst() const {
	return CONST;
}

bool Type::isExtern() const {
	return EXTERN;
}

std::string Type::toStr() const {
	if( TC == Builtin ) {
		return "Builtin";
	} else if ( TC == Pointer ) {
		return "Pointer";
	} else if ( TC == Typedef ) {
		return "Typedef";
	} else if ( TC == Function ) {
		return "Function";
	} else if ( TC == Record ) {
		return "Record";
	} else if ( TC == Array ) {
		return "Array";
	} else {
		return "Enum";
	}
}

std::string Type::TCToStr(TypeClass t){
	if( t == Builtin ) {
		return "Builtin";
	} else if ( t == Pointer ) {
		return "Pointer";
	} else if ( t == Typedef ) {
		return "Typedef";
	} else if ( t == Function ) {
		return "Function";
	} else if ( t == Record ) {
		return "Record";
	} else if ( t == Array ) {
		return "Array";
	} else {
		return "Enum";
	}
}

bool Type::isBuiltin() {
	return false;
}
bool Type::isPointer() {
	return false;
}
bool Type::isTypedef() {
	return false;
}
bool Type::isFunction() {
	return false;
}
bool Type::isRecord() {
	return false;
}
bool Type::isArray() {
	return false;
}
bool Type::isEnum() {
	return false;
}


//builtins
BuiltinType::BuiltinType(BuiltinType::Kind k, unsigned int s, bool c, bool e)
:Type(Builtin, c, e), kind(k), size(s)
{}

BuiltinType::Kind BuiltinType::getKind() const {
	return kind;
}

unsigned int BuiltinType::getSize() const {
	return size;
}

std::string BuiltinType::kindToStr( BuiltinType::Kind k ) {
	if( UChar == k )
		return "UChar";
	if( UShort == k )
		return "UShort";
	if( UInt == k )
		return "UInt";
	if( ULong == k )
		return "ULong";
	if( ULongLong == k )
		return "ULongLong";
	if( Char == k )
		return "Char";
	if( Short == k )
		return "Short";
	if( Int == k )
		return "Int";
	if( Long == k )
		return "Long";
	if( LongLong == k )
		return "LongLong";
	if( Float == k )
		return "Float";
	if( Double == k )
		return "Double";
	return "Void";
}

bool BuiltinType::isBuiltin() {
	return true;
}

//pointers
PointerType::PointerType( Type *t, bool c, bool e)
:Type(Pointer, c, e), type(t)
{}

Type* PointerType::getType() const {
	return type;
}

unsigned int PointerType::getSize() const {
	return 4;//32bit pointer size assumed. TODO: check this
}

bool PointerType::isPointer() {
	return true;
}

//typedefs
TypedefType::TypedefType( Type* t, bool c, bool e )
:Type(Typedef, c, e), base(t)
{}

Type* TypedefType::desugar() const {
	return base;
}

unsigned int TypedefType::getSize() const {
	return base->getSize();
}

bool TypedefType::isTypedef() {
	return true;
}

//functions
FunctionType::FunctionType( Type *r, bool c, bool e )
:Type(Function, c, e), ret(r)
{}

Type* FunctionType::getReturnType() const {
	return ret;
}

void FunctionType::addArg( Type* f ) {
	args.push_back(f);
}

unsigned int FunctionType::getArgCount() const {
	return args.size();
}

Type* FunctionType::getArgType(unsigned int n) const {
	return args.at(n);
}

unsigned int FunctionType::getSize() const {
	//TODO: figure out what makes sense
	//		need to account for code size maybe? really not sure on this one
	return 0;
}

bool FunctionType::isFunction() {
	return true;
}

//record types
RecordType::RecordType( RecordType::Kind k, std::vector<std::string> names,
						std::vector<Type*> types, bool c, bool e )
:Type(Record, c, e), fields(names), kind(k)
{
	size = 0;
	for( unsigned int i = 0; i < names.size(); ++i ) {
		fieldTypes[ names[i] ] = types[i];
		size += types[i]->getSize();
	}
}

RecordType::Kind RecordType::getKind() const {
	return kind;
}

unsigned int RecordType::getSize() const {
	return size;
}

bool RecordType::fieldExists( const std::string& f) const {
	return fieldTypes.count(f);
}

Type* RecordType::getFieldType( const std::string& f) {
	return fieldTypes[f];
}

bool RecordType::isRecord() {
	return true;
}

//array types
ArrayType::ArrayType( Type *t, unsigned int n, bool c, bool e )
:Type(Array, c, e),type(t), capacity(n)
{}

Type* ArrayType::getType() const {
	return type;
}

unsigned int ArrayType::getCapacity() const {
	return capacity;
}

unsigned int ArrayType::getSize() const {
	return capacity * type->getSize();
}

bool ArrayType::isArray() {
	return true;
}

//enumerated types
EnumType::EnumType( bool e )
:Type(Enum, true, e), count(0)
{}

int EnumType::getCount() {
	return count;
}

void EnumType::setCount( int newCount ) {
	count = newCount;
}

void EnumType::incrCount() {
	count++;
}

bool EnumType::isEnum() {
	return true;
}
}//dtcc
