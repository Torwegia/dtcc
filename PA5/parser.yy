%{
	#include <stdio.h>
	#include <string>
	#include <vector>
	#include "type.h"
	#include "symbol.h"
	#include "symbolTable.h"
	#include "driver.h"
	#include "scanner.h"
	#include "decl.h"

	//Things which help the parser

	//#include "parserFlags.hpp"

	//Global vars
	//TODO: CONSIDER MAKING A STACK
	dtcc::Decl curDecl;
	
	
	//#include "ast.hpp"
	std::string CUR_ID;
	dtcc::Parser::location_type CUR_LOC;


%}

%require "2.3"

%debug

%define "parser_class_name" "Parser"

%locations

%initial-action
{
	@$.begin.filename = @$.end.filename = &driver.filename;
};

%union{
unsigned long int intVal;
float floatVal;
char *stringVal;
dtcc::BuiltinType::Kind kind;
}

%parse-param { class Driver& driver }


%error-verbose


%defines

%skeleton "lalr1.cc"

%name-prefix="dtcc"


%token END 0 "end of file"
%token IDENTIFIER 
%token <intVal> INTEGER_CONSTANT
%token <floatVal> FLOATING_CONSTANT 
%token CHARACTER_CONSTANT ENUMERATION_CONSTANT 
%token STRING_LITERAL 
%token SIZEOF
%token PTR_OP 
%token INC_OP DEC_OP 
%token LEFT_OP RIGHT_OP 
%token LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP 
%token MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN SUB_ASSIGN 
%token LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN XOR_ASSIGN OR_ASSIGN
%token OPEN_CURLY CLOSE_CURLY OPEN_PAREN CLOSE_PAREN OPEN_SQUARE CLOSE_SQUARE
%token PLUS MINUS DIV MUL
%token COLON SEMI COMMA DOT TILDE Q_MARK
%token AND OR NOT MOD XOR EQUAL
%token LE GE 
%token TYPEDEF_NAME

%token TYPEDEF EXTERN STATIC AUTO REGISTER
%token CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID
%token STRUCT UNION ENUM ELIPSIS RANGE

%token CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%start translation_unit


%{

//%type <kind> declaration_specifiers type_specifier type_qualifier

#undef yylex
#define yylex driver.lexer->lex

%}

%%


translation_unit
    : external_declaration
    | translation_unit external_declaration
    ;

external_declaration
    : function_definition
    | declaration {
		if ( curDecl.isFunction() ) {
			curDecl.setProto();
		}
		bool success;
		success = curDecl.End();
		if( !success ) { //get the location of the badness
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
    ;

function_definition
    : declarator {
		bool success = curDecl.End();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
		SymbolTable::getInstance()->pushScope();
	} compound_statement {
		SymbolTable::getInstance()->popScope();
	}
    | declarator declaration_list {
		bool success = curDecl.End();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
		SymbolTable::getInstance()->pushScope();
		//TODO:push the parameters into the scope somehow
	} compound_statement {
		SymbolTable::getInstance()->popScope();
	}
    | declaration_specifiers declarator {
		bool success = curDecl.End();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
		SymbolTable::getInstance()->pushScope();
	} compound_statement {
		SymbolTable::getInstance()->popScope();
	}
    | declaration_specifiers declarator declaration_list {
		bool success = curDecl.End();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
		SymbolTable::getInstance()->pushScope();
	} compound_statement {
		SymbolTable::getInstance()->popScope();
	}
    ;

declaration
    : declaration_specifiers {
		bool success = curDecl.applyMods();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	} SEMI 
    | declaration_specifiers init_declarator_list SEMI 
    ;

declaration_list
    : declaration {
		bool success;
		success = curDecl.End();
		if( !success ) { //get the location of the badness
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
    | declaration_list declaration {
		bool success;
		success = curDecl.End();
		if( !success ) { //get the location of the badness
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
    ;

declaration_specifiers
    : storage_class_specifier {
		bool success = curDecl.applyMods();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
    | storage_class_specifier declaration_specifiers {
		bool success = curDecl.applyMods();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
    | type_specifier {
		bool success = curDecl.applyMods();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
	| type_specifier declaration_specifiers {
		bool success = curDecl.applyMods();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
    | type_qualifier {
		bool success = curDecl.applyMods();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
    | type_qualifier declaration_specifiers {
		bool success = curDecl.applyMods();
		if( !success ) {
			error( curDecl.getErrorLoc(), curDecl.getErrorStr() );
		}
	}
    ;

storage_class_specifier
    : AUTO
    | REGISTER
    | STATIC
    | EXTERN
    | TYPEDEF {
		bool success = curDecl.setTypedef();
		if( !success ) {
			error( yylloc, "storage_class_specifier typedef is invalid here" );
		}
	}
    ;

type_specifier
    : VOID{	
		curDecl.pushTypeClass( dtcc::Type::Builtin, yylloc );
		curDecl.pushKind( dtcc::BuiltinType::Void, yylloc );
	}
    | CHAR {
		curDecl.pushTypeClass( dtcc::Type::Builtin, yylloc );
		curDecl.pushKind( dtcc::BuiltinType::Char, yylloc );
	}
    | SHORT {
		curDecl.pushTypeClass( dtcc::Type::Builtin, yylloc );
		curDecl.pushKind( dtcc::BuiltinType::Short, yylloc );
	}
    | INT {
		curDecl.pushTypeClass( dtcc::Type::Builtin, yylloc );
		curDecl.pushKind( dtcc::BuiltinType::Int, yylloc );
	}
    | LONG {
		curDecl.pushTypeClass( dtcc::Type::Builtin, yylloc );
		curDecl.pushKind( dtcc::BuiltinType::Long, yylloc );
	}
    | FLOAT {
		curDecl.pushTypeClass( dtcc::Type::Builtin, yylloc );
		curDecl.pushKind( dtcc::BuiltinType::Float, yylloc );
	}
    | DOUBLE {
		curDecl.pushTypeClass( dtcc::Type::Builtin, yylloc );
		curDecl.pushKind( dtcc::BuiltinType::Double, yylloc );
	}
    | SIGNED {
		if( curDecl.isSignSet() )
		{
			error(yylloc,"sign is already set");
		}
		curDecl.setSigned();
	}
    | UNSIGNED { 
		if(curDecl.isSignSet())
		{
			error(yylloc,"sign is already set");
		}
		curDecl.setUnsigned();
	}
    | struct_or_union_specifier {
		std::cout << "STRUCTS AND UNIONS NOT YET IMPLEMENTED" << std::endl << std::endl;
		curDecl.setSkip();
	}
    | enum_specifier {
		std::cout << "ENUMS NOT YET IMPLEMENTED" << std::endl << std::endl;
		curDecl.setSkip();
	}
    | TYPEDEF_NAME {
		CUR_ID = std::string(yylval.stringVal);
		CUR_LOC = yylloc;
		curDecl.pushId( CUR_ID, CUR_LOC );
		
		curDecl.pushTypeClass( dtcc::Type::Typedef, yylloc );
		curDecl.pushKind( dtcc::BuiltinType::Void, yylloc );
	}
    ;

type_qualifier
    : CONST {
		//set const
		//might need to wait for AST before this important
	}
    | VOLATILE 
    ;

struct_or_union_specifier
    : struct_or_union identifier OPEN_CURLY struct_declaration_list CLOSE_CURLY {
		curDecl.setSkip();
		//named struct
	}
    | struct_or_union OPEN_CURLY struct_declaration_list CLOSE_CURLY {
		curDecl.setSkip();
		//unnamed struct
	}
    | struct_or_union identifier {
		curDecl.setSkip();
		//lookup the struct
	}
    ;

struct_or_union
    : STRUCT {
		curDecl.setSkip();
		//set curDecl to struct
	}
    | UNION {
		curDecl.setSkip();
		//set curDecl to union
	}
    ;

struct_declaration_list
    : struct_declaration {
		curDecl.setSkip();
	}
    | struct_declaration_list {
		curDecl.setSkip();
	} struct_declaration
    ;

init_declarator_list
    : init_declarator
    | init_declarator_list COMMA init_declarator
    ;

init_declarator
    : declarator 
    | declarator EQUAL initializer 
    ;

struct_declaration
    : specifier_qualifier_list {
		curDecl.setSkip();
	} struct_declarator_list SEMI 
    ;

specifier_qualifier_list
    : type_specifier
    | type_specifier specifier_qualifier_list
    | type_qualifier
    | type_qualifier specifier_qualifier_list
    ;

struct_declarator_list
    : struct_declarator {
		curDecl.setSkip();
	}
    | struct_declarator_list { 
		curDecl.setSkip();
	} COMMA struct_declarator 
    ;

struct_declarator
    : declarator {
		curDecl.setSkip();
		//The id of some known type
		//possible errors here
	}
    | COLON constant_expression {
		curDecl.setSkip();
		//bitfield?
		//need to check if the declaration up to this point is a in of some sort, might not need to implement
	}
    | declarator COLON constant_expression {
		curDecl.setSkip();
		//named bitfield?
		//need to check if the declaration up to this point is a in of some sort, might not need to implement
	}
    ;

enum_specifier
    : ENUM OPEN_CURLY enumerator_list CLOSE_CURLY {
		curDecl.setSkip();
		//unnamed enum
	}
    | ENUM identifier OPEN_CURLY enumerator_list CLOSE_CURLY {
		curDecl.setSkip();
		//named enum
	}
    | ENUM identifier {
		curDecl.setSkip();
		//forward declaring an enum
	}
    ;

enumerator_list
    : enumerator {
		curDecl.setSkip();
		//add an id and possibly a new counter
	}
    | enumerator_list COMMA enumerator {
		curDecl.setSkip();
		//add an id and possibly a new counter
	}
    ;

enumerator
    : identifier
    | identifier EQUAL constant_expression
    ;

declarator
    : direct_declarator
    | pointer direct_declarator {
		//set the pointer type
		//TODO: Replace current typeclass with pointer and push the old typeclass into a vector to be dealt with later
		std::cout << "POINTERS NOT YET IMPLEMENTED" << std::endl;
		curDecl.setSkip();
	}
    ;

direct_declarator
    : identifier {
		curDecl.pushId( CUR_ID, CUR_LOC );
	}
    | OPEN_PAREN declarator CLOSE_PAREN {
		//a wrapped present?
	}
    | direct_declarator OPEN_SQUARE CLOSE_SQUARE {
		//array
		//no size here
		curDecl.setArray();
		curDecl.setArraySize(1);
	}
    | direct_declarator OPEN_SQUARE constant_expression CLOSE_SQUARE {
		//array
		//constant expr has the size
		curDecl.setArray();
		curDecl.setArraySize(yylval.intVal);
	}
    | direct_declarator OPEN_PAREN CLOSE_PAREN {
		curDecl.setFunction();
	}
    | direct_declarator OPEN_PAREN parameter_type_list {
		//function decl stuff
		curDecl.setFunction();
	} CLOSE_PAREN
    | direct_declarator OPEN_PAREN identifier_list {
		//Look up for a function 
		//or a object creation ?
		//TODO: REMOVE WHEN IMPLEMENTED
		std::cout << "function lookup" << std::endl;
	} CLOSE_PAREN
    ;

pointer
    : MUL
    | MUL type_qualifier_list {
		//constness?
	}
    | MUL pointer
    | MUL type_qualifier_list pointer {
		//constness?
	}
    ;

type_qualifier_list
    : type_qualifier
    | type_qualifier_list type_qualifier
    ;

parameter_type_list
    : parameter_list
    | parameter_list COMMA ELIPSIS
    ;

parameter_list
    : parameter_declaration 
    | parameter_list COMMA {
		curDecl.resetMods();
	} parameter_declaration
    ;

parameter_declaration
    : declaration_specifiers declarator
    | declaration_specifiers
    | declaration_specifiers abstract_declarator
    ;

identifier_list
    : identifier
    | identifier_list COMMA identifier
    ;

initializer
    : assignment_expression
    | OPEN_CURLY initializer_list CLOSE_CURLY
    | OPEN_CURLY initializer_list COMMA CLOSE_CURLY
    ;

initializer_list
    : initializer
    | initializer_list COMMA initializer
    ;

type_name
    : specifier_qualifier_list
    | specifier_qualifier_list abstract_declarator
    ;

abstract_declarator
    : pointer
    | direct_abstract_declarator
    | pointer direct_abstract_declarator
    ;

direct_abstract_declarator
    : OPEN_PAREN abstract_declarator CLOSE_PAREN
    | OPEN_SQUARE CLOSE_SQUARE
    | OPEN_SQUARE constant_expression CLOSE_SQUARE
    | direct_abstract_declarator OPEN_SQUARE CLOSE_SQUARE
    | direct_abstract_declarator OPEN_SQUARE constant_expression CLOSE_SQUARE
    | OPEN_PAREN CLOSE_PAREN
    | OPEN_PAREN parameter_type_list CLOSE_PAREN
    | direct_abstract_declarator OPEN_PAREN CLOSE_PAREN
    | direct_abstract_declarator OPEN_PAREN parameter_type_list CLOSE_PAREN
    ;

statement
    : labeled_statement
    | compound_statement
    | expression_statement
    | selection_statement
    | iteration_statement
    | jump_statement
    ;

labeled_statement
    : identifier COLON statement
    | CASE constant_expression COLON statement
    | DEFAULT COLON statement
    ;

expression_statement
    : SEMI
    | expression SEMI
    ;

compound_statement
    : OPEN_CURLY CLOSE_CURLY
    | OPEN_CURLY statement_list CLOSE_CURLY
    | OPEN_CURLY declaration_list {
		//Swap to lookup mode
	} CLOSE_CURLY
    | OPEN_CURLY declaration_list {
		//Swap to lookup mode
	} statement_list CLOSE_CURLY
    ;

statement_list
    : statement
    | statement_list statement
    ;

if_statement
    : IF OPEN_PAREN expression CLOSE_PAREN {
		SymbolTable::getInstance()->pushScope();
	} statement {
		SymbolTable::getInstance()->popScope();
	}
	;

selection_statement
    : if_statement
    | if_statement ELSE {
		SymbolTable::getInstance()->pushScope();
	} statement {
		SymbolTable::getInstance()->popScope();
	}
    | SWITCH OPEN_PAREN expression CLOSE_PAREN {
		SymbolTable::getInstance()->pushScope();
	} statement {
		SymbolTable::getInstance()->pushScope();
	}
    ;

iteration_statement
    : WHILE OPEN_PAREN expression CLOSE_PAREN statement
    | DO statement WHILE OPEN_PAREN expression CLOSE_PAREN SEMI
    | FOR OPEN_PAREN SEMI SEMI CLOSE_PAREN statement
    | FOR OPEN_PAREN SEMI SEMI expression CLOSE_PAREN statement
    | FOR OPEN_PAREN SEMI expression SEMI CLOSE_PAREN statement
    | FOR OPEN_PAREN SEMI expression SEMI expression CLOSE_PAREN statement
    | FOR OPEN_PAREN expression SEMI SEMI CLOSE_PAREN statement
    | FOR OPEN_PAREN expression SEMI SEMI expression CLOSE_PAREN statement
    | FOR OPEN_PAREN expression SEMI expression SEMI CLOSE_PAREN statement
    | FOR OPEN_PAREN expression SEMI expression SEMI expression CLOSE_PAREN statement
    ;

jump_statement
    : GOTO identifier SEMI
    | CONTINUE SEMI
    | BREAK SEMI
    | RETURN SEMI
    | RETURN expression SEMI
    ;

expression
    : assignment_expression
    | expression COMMA assignment_expression
    ;

assignment_expression
    : conditional_expression
    | unary_expression assignment_operator assignment_expression
    ;

assignment_operator
    : EQUAL
    | MUL_ASSIGN
    | DIV_ASSIGN
    | MOD_ASSIGN
    | ADD_ASSIGN
    | SUB_ASSIGN
    | LEFT_ASSIGN
    | RIGHT_ASSIGN
    | AND_ASSIGN
    | XOR_ASSIGN
    | OR_ASSIGN
    ;

conditional_expression
    : logical_or_expression
    | logical_or_expression Q_MARK expression COLON conditional_expression
    ;

constant_expression
    : conditional_expression
    ;

logical_or_expression
    : logical_and_expression
    | logical_or_expression OR_OP logical_and_expression
    ;

logical_and_expression
    : inclusive_or_expression
    | logical_and_expression AND_OP inclusive_or_expression
    ;

inclusive_or_expression
    : exclusive_or_expression
    | inclusive_or_expression OR exclusive_or_expression
    ;

exclusive_or_expression
    : and_expression
    | exclusive_or_expression XOR and_expression
    ;

and_expression
    : equality_expression
    | and_expression AND equality_expression
    ;

equality_expression
    : relational_expression
    | equality_expression EQ_OP relational_expression
    | equality_expression NE_OP relational_expression
    ;

relational_expression
    : shift_expression
    | relational_expression LE shift_expression
    | relational_expression GE shift_expression
    | relational_expression LE_OP shift_expression
    | relational_expression GE_OP shift_expression
    ;

shift_expression
    : additive_expression
    | shift_expression LEFT_OP additive_expression
    | shift_expression RIGHT_OP additive_expression
    ;

additive_expression
    : multiplicative_expression
    | additive_expression PLUS multiplicative_expression
    | additive_expression MINUS multiplicative_expression
    ;

multiplicative_expression
    : cast_expression
    | multiplicative_expression MUL cast_expression
    | multiplicative_expression DIV cast_expression
    | multiplicative_expression MOD cast_expression
    ;

cast_expression
    : unary_expression
    | OPEN_PAREN type_name CLOSE_PAREN cast_expression
    ;

unary_expression
    : postfix_expression
    | INC_OP unary_expression
    | DEC_OP unary_expression
    | unary_operator cast_expression
    | SIZEOF unary_expression
    | SIZEOF OPEN_PAREN type_name CLOSE_PAREN
    ;

unary_operator
    : AND
    | MUL
    | PLUS
    | MINUS
    | TILDE
    | NOT
    ;

postfix_expression
    : primary_expression
    | postfix_expression OPEN_SQUARE expression CLOSE_SQUARE
    | postfix_expression OPEN_PAREN CLOSE_PAREN
    | postfix_expression OPEN_PAREN argument_expression_list CLOSE_PAREN
    | postfix_expression DOT identifier
    | postfix_expression PTR_OP identifier
    | postfix_expression INC_OP
    | postfix_expression DEC_OP
    ;

primary_expression
    : identifier
    | constant
    | string
    | OPEN_PAREN expression CLOSE_PAREN
    ;

argument_expression_list
    : assignment_expression
    | argument_expression_list COMMA assignment_expression
    ;

constant
    : INTEGER_CONSTANT
    | CHARACTER_CONSTANT
    | FLOATING_CONSTANT
    | ENUMERATION_CONSTANT
    ;

string
    : STRING_LITERAL
    ;

identifier
    : IDENTIFIER {
		CUR_ID = std::string(yylval.stringVal);
		CUR_LOC = yylloc;
	}
    ;

%%

void dtcc::Parser::error(const Parser::location_type& l, const std::string& m){
	driver.error(l, m);
}
