#ifndef ERROR_LOGGER_HPP
#define ERROR_LOGGER_HPP
#include <fstream>
#include <string>
#include <vector>

#include "type.h"
#include "parser.h"
namespace dtcc
{
	class ErrorLogger
	{
		private:
			ErrorLogger();
			static void fill(const char fc, unsigned int line, unsigned int col ) {
				for(unsigned int i = 0; i < col; i++) {
					if( (file[line])[i] == '\t' ) {
						for( int j = 0; j < ( 8 - i%8 ); j++ ) {
							std::cerr << fc;
						}
					} else {
						std::cerr << fc;
					}
				}
			}

			static std::vector< std::string > file;

		public:
			static void init( std::string fn ) {
				std::ifstream fin(fn.c_str());
				std::string t;
				std::getline(fin, t);
				while( fin.good() ) {
					file.push_back(t);
					
					std::getline(fin, t);
				}
			}

			static void error( const Parser::location_type& l, const std::string& m) {
				std::cerr << "Error: " << m << std::endl;
				//then print the line with an arrow to the error
				unsigned int lineNo = l.begin.line - 1;//1-indexed
				unsigned int colNo = l.begin.column - 1;//1-indexed

				std::cerr << "At " << l << std::endl;

				std::cerr << file[lineNo] << std::endl;
				fill('~', lineNo, colNo);
				std::cerr << "^" << std::endl;
				exit(1);
			}
	};
}

#endif
