#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <iostream>
#include <fstream>
#include <string>

#include "driver.h"

int main(int argc, char *argv[]) {
	//argument handling
	int debug;
	std::string input, output;

	po::variables_map vm;

	po::options_description desc("Options");
	desc.add_options()
		("help", "Print this message")
		("debug,d", po::value<int>(&debug)->default_value(0), "Specifies the debug level")
		("output-file,o", po::value<std::string>(&output), "Specifies the output file name")
		("input-file,i", po::value<std::string>(&input)->required(), "The source file name");

	po::positional_options_description p;
	p.add("input-file", -1);

	po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

	if( vm.count("help") ) {
		std::cout << "Devyani/Tor C Compiler" << std::endl
				  << desc << std::endl;
		return 0;
	}


	try {
		po::notify(vm);
	} catch(po::error &e) {
		std::cerr << "Error: " << e.what() << std::endl;
		std::cerr << desc << std::endl;
		return -1;
	}

	//Use the arguments
	dtcc::Driver *compiler;
	if( vm.count("output-file") )
		compiler = new dtcc::Driver(debug, input, output);
	else
		compiler = new dtcc::Driver(debug, input);

	bool success = compiler->parse();
	if(success) {
		std::cout << "It worked!" << std::endl;
	}

	return 0;
}
