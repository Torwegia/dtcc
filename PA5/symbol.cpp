#include "symbol.h"

namespace dtcc {

Symbol::Symbol( const Parser::location_type& l, Type *t, const std::string& tn, bool c)
:loc(l), type(t), typeName(tn), incomplete(c)
{}

Parser::location_type Symbol::getLocation() const {
	return loc;
}

Type* Symbol::getType() const {
	return type;
}

std::string Symbol::getTypeName() const {
	return typeName;
}

bool Symbol::isIncomplete() const {
	return incomplete;
}

void Symbol::setComplete() {
	incomplete = true;
}

}//dtcc
