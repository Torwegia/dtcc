#pragma once

#ifndef YY_DECL

#define YY_DECL									\
	dtcc::Parser::token_type					\
	dtcc::Scanner::lex(							\
		dtcc::Parser::semantic_type* yylval,	\
		dtcc::Parser::location_type* yylloc		\
	)

#endif

#ifndef __FLEX_LEXER_H
#define yyFlexLexer DTCCFlexLexer
#include "FlexLexer.h"
#undef yyFlexLexer
#endif

#include "type.h"
#include "symbol.h"
#include "parser.h"

namespace dtcc {

class Scanner : public DTCCFlexLexer {
	public:
		Scanner(std::istream *arg_yyin = 0, std::ostream *arg_yyout = 0);

		virtual ~Scanner();

		virtual Parser::token_type lex(Parser::semantic_type* yylval, Parser::location_type* yylloc);

		void set_debug(bool b);
};
}
