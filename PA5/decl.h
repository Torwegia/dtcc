#pragma once

#include "type.h"
#include "symbol.h"
#include "symbolTable.h"

#include <iostream>
#include <vector>
#include <string>
#include <utility>

namespace dtcc {

//Holds all the information for a declaration
class Decl {
	public:
		enum Mode {
			Function,
			Array,
			Typedef,
			Default
		};

		enum Sign {
			Signed,
			Unsigned,
			NotSet
		};

		Decl();

		void reset();
		void setSkip();

		bool End();//Evaluate the declaration

		//evil typedef handling
		bool setTypedef();
		bool getTypedef();

		//functions
		void setFunction();
		bool isFunction();
		void setProto();

		//sign handling
		bool isSignSet();
		void setSigned();
		void setUnsigned();
		Sign getSign();

		bool applyMods();
		void resetMods();

		//arrays
		bool isArray();
		void setArray();
		void setArraySize(int s);


		void pushId( const std::string&, Parser::location_type );
		void pushTypeClass( Type::TypeClass, Parser::location_type );
		void pushKind( BuiltinType::Kind, Parser::location_type );

		//error handling
		std::string getErrorStr();
		Parser::location_type getErrorLoc();


	private:
		Mode mode;
		bool TDef;
		bool proto;
		bool skip;
		
		bool intConsumed;

		//array size

		Sign sign;
		bool constness;
		unsigned int size;//what place to start collapsing kinds and typeclasses from

		//error handling
		std::string errorStr;
		Parser::location_type errorLoc;

		/*
		 * Holds all the identifiers
		 * in default mode these are just the ids of the new symbols
		 * in typedef mode these are the id of the types in the typedef
		 * in function mode these are the return type id, the functions id, argument types
		 *
		 */
		std::vector< std::pair< std::string, Parser::location_type > > ids;
		std::vector< std::pair< Type::TypeClass, Parser::location_type > > typeclasses;
		std::vector< std::pair< BuiltinType::Kind, Parser::location_type > > kinds;
		std::vector< Sign > signs;
		std::vector< int > elements;



		bool collapseTypeClass( std::pair< Type::TypeClass, Parser::location_type > );
		bool collapseKind( std::pair< BuiltinType::Kind, Parser::location_type > );

		std::string generateTypeClassErr( unsigned int );
		std::string generateKindErr( unsigned int );
		std::string genTypename( BuiltinType::Kind, Sign );

		bool insertFunction();
		bool insertTypedef();
		bool insertArray( Type*, const std::string&, const Parser::location_type&,
						 int, const std::string& );
		bool insertUserType(const std::string&, const std::string&, const Parser::location_type& );
		bool insertBuiltinType( const std::string&, const Parser::location_type& );

};

}
