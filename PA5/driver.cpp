#include "driver.h"
#include "scanner.h"
#include "error.hpp"
#include "symbolTable.h"
#include <fstream>

namespace dtcc {


Driver::Driver(const std::string& fname)
:debug_level(0),use_out(false)
{
	filename = fname;
}

Driver::Driver(int dLevel, const std::string& fname)
:debug_level(dLevel)
{
	filename = fname;
}

Driver::Driver(int dLevel, const std::string& fname
			  , const  std::string& oname)
:debug_level(dLevel), output(oname),use_out(true)
{
	filename = fname;
}

bool Driver::parse() {
	//suck the file in
	ErrorLogger::init(filename);

	std::ifstream fin(filename.c_str());

	if(!fin.good()) {
		error( "File not found");
		return false;
	}


	//set up the lexer
	if( !use_out ) {
		lexer = new Scanner(&fin);
	} else {
		std::ofstream fout(output.c_str());
		lexer = new Scanner(&fin, &fout);
	}
	if( debug_level == 1 || debug_level > 3 ) {
		lexer->set_debug(true);
	}

	//start the parse
	Parser parser(*this);
	if( debug_level == 2 || debug_level > 3 ) {
		parser.set_debug_level(true);
	}

	if(debug_level >2)
	{
		SymbolTable::getInstance()->set_debug(true);
	}
	return parser.parse() == 0;
}

void Driver::error(const class location& l, const std::string& m) {
	ErrorLogger::error(l,m);
}

void Driver::error(const std::string& m) {
	std::cerr << "Error: " << m << std::endl;
	exit(1);
}

}
