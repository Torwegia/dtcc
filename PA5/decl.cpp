#include "decl.h"

#define baseTC typeclasses[size].first
#define baseK  kinds[size].first
#define Pair std::make_pair
typedef std::pair<dtcc::Type::TypeClass, dtcc::Parser::location_type> typeclass_p;
typedef std::pair<dtcc::BuiltinType::Kind, dtcc::Parser::location_type> kind_p;

namespace dtcc {

Decl::Decl() {
	reset();
}

void Decl::reset() {
	mode = Default;
	TDef = false;
	intConsumed = false;

	sign = NotSet;
	constness = false;

	proto = false;
	skip = false;

	ids.clear();
	typeclasses.clear();
	kinds.clear();
	signs.clear();

	elements.clear();

	size = 0;
}

void Decl::setSkip() {
	skip = true;
}

bool Decl::applyMods() {
	if( !skip ) {
		//TODO::IMPLEMENT structs and other nonsense;
		//apply const and everything to the immediately previous types

		//TODO: CHANGE TO CHECK IF BOTH ARE BUILTIN BEFORE COLLAPSING

		for( unsigned int i = kinds.size() - 1; i > size; i-- ) {
			bool success = collapseKind( kinds[i] );
			if(!success) { //handle type class error
				errorStr = generateKindErr(i);
				errorLoc = typeclasses[i].second;
				return false;
			}
			kinds.pop_back();//it successfully collapsed so remove it
			typeclasses.pop_back();
		}


		signs.push_back(sign);
		size = kinds.size();//set a marker at the end of the kinds stack

		for( unsigned int i = signs.size() - 1; i >= size; i-- )
		{
			if( signs[size] == NotSet && signs[i] != signs[size] ) {
				signs[size] = signs[i];
			}
			signs.pop_back();
		}
	}

	resetMods();
	return true;
}

void Decl::resetMods() {
	sign = NotSet;
	constness = false;
}

bool Decl::End() {
	if ( !skip ) {
		if ( mode == Typedef ) {
			if(!insertTypedef() ) 
				return false;
		} else if ( mode == Function ) {
			//let insertFunction handle generating error stuff
			//too many ways to fail
			if( !insertFunction() )
				return false;
		} else if ( mode == Array ) {
			unsigned int i;
			std::string typeName;
			if(typeclasses[0].first == Type::Typedef) {
				i = 1;
				typeName = ids[0].first;
			} else {
				i = 0;
				typeName = genTypename( kinds[0].first, signs[0] );
			}
			Type *base = SymbolTable::getInstance()->findType(typeName);
			if( !base ) {
				errorStr = typeName + " does not exist as a type";
				errorLoc = kinds[i].second;
				return false;
			}
			for( ; i < ids.size(); ++i) {
				if( !insertArray( base, ids[i].first, ids[i].second, elements[i], typeName ) ) {
					return false;
				}
			}
		} else { //Default
			if(typeclasses[0].first == Type::Builtin ) {
				for( unsigned int i = 0; i < ids.size(); ++i) {
					if( !insertBuiltinType( ids[i].first, ids[i].second ) ) {
						//prepare error str and loc;
						errorStr = "Shadowed variable name, should be a warning, but we play for keeps";
						errorLoc = ids[i].second;
						return false;
					}
				}
			} else if( typeclasses[0].first == Type::Typedef ) {
				//the name of the type
				std::string typeName = ids[0].first;
				for( unsigned int i = 1; i < ids.size(); ++i) {
					if( !insertUserType( typeName, ids[i].first, ids[i].second ) ) {
						errorStr = "Shadowed variable name, should be a warning, but we play for keeps";
						errorLoc = ids[i].second;
						return false;
					}
				}
			}
		}
	}

	reset();
	return true;
}

bool Decl::setTypedef() {
	if( mode == Default ) {
		mode = Typedef;
		return true;
	} else {
		return false;
	}
}

bool Decl::getTypedef() {
	return mode == Typedef;
}

bool Decl::isFunction() {
	return mode == Function;
}

void Decl::setProto() {
	proto = true;
}

void Decl::setFunction() {
	mode = Function;
}


bool Decl::isSignSet() {
	return sign != NotSet;
}

void Decl::setSigned() {
	sign = Signed;
}

void Decl::setUnsigned() {
	sign = Unsigned;
}

Decl::Sign Decl::getSign() {
	return sign;
}

bool Decl::isArray() {
	return mode == Array;
}

void Decl::setArray() {
	mode = Array;
}

void Decl::setArraySize( int s ) {
	elements.push_back(s);
}

//id type kind stacks and stack managemet below
void Decl::pushId( const std::string& s, Parser::location_type loc ) {
	ids.push_back( Pair( s, loc ) );
}

void Decl::pushTypeClass( Type::TypeClass tc, Parser::location_type loc ) {
	typeclasses.push_back( Pair( tc, loc ) );
}

void Decl::pushKind( BuiltinType::Kind k, Parser::location_type loc ) {
	kinds.push_back( Pair( k, loc ) );
}

bool Decl::collapseTypeClass( typeclass_p pair ) {
	//TODO::FLESH THIS OUT Structs and enums //process whether we can take another type class
	if( baseK == BuiltinType::Short ||
		baseK == BuiltinType::Int ||
		baseK == BuiltinType::Long ||
		baseK == BuiltinType::LongLong) { //these types allow continuations
												   //meaning we can try to continue
		return true;
	} else {
		return false;
	}
}

bool Decl::collapseKind( kind_p pair ) {
	BuiltinType::Kind k = pair.first;
	if( ( baseK == BuiltinType::Short ||
		  baseK == BuiltinType::Long ) &&
		k == BuiltinType::Int ) { //long int or short int, devolve and set a flag
		if( intConsumed ) { //long int int - error out
			return false;
		} else { //long int -- good
			intConsumed = true;
			return true;
		}
	} else if( baseK == BuiltinType::Long && k == baseK ) {//long long or long int long 
		baseK = BuiltinType::LongLong;
		return true;
	} else if( baseK == BuiltinType::LongLong ) { //long long x
		if( k == BuiltinType::Int ) { //long long int or long long int int
			if( intConsumed ) { //long long int int - error
				return false;
			} else {
				intConsumed = true;
				return true;
			}
		} else { //long long void or something silly
			return false;
		}
	} else if( baseK == BuiltinType::Int && k == BuiltinType::Long) {
		intConsumed = true;
		baseK = k;
		return true;
	} else {
		return false;
	}
}

std::string Decl::generateTypeClassErr( unsigned int sl ) {
	std::string part1 = "typeclass ";
	std::string tc = Type::TCToStr(typeclasses[sl].first);
	std::string part2 = " is invalid in this location";

	return part1 + tc + part2;
}

std::string Decl::generateKindErr( unsigned int sl ) {
	std::string part1 = "builtin type ";
	std::string kind = BuiltinType::kindToStr(kinds[sl].first);
	std::string part2 = " is invalid in this location";

	return part1 + kind + part2;
}

std::string Decl::genTypename( BuiltinType::Kind k, Sign s ) {
	std::string typeName = dtcc::BuiltinType::kindToStr(k), u = "U";

	if(s == dtcc::Decl::Unsigned )
		typeName = u + typeName;

	return typeName;
}


std::string Decl::getErrorStr() {
	return errorStr;
}

Parser::location_type Decl::getErrorLoc() {
	return errorLoc;
}

bool Decl::insertFunction() {
	//check if its there
	std::string key = ids[0].first;
	Symbol *protoDecl = SymbolTable::getInstance()->findSymbol(key);
	if( !proto && protoDecl != NULL ) {
		//check if our implementation matches the proto
		if( !protoDecl->getType()->isFunction() ) {
			errorLoc = ids[0].second;
			errorStr = "The symbol " + ids[0].first + " is not a function";
			return false;
		} else if( !protoDecl->isIncomplete() ) {
			errorLoc = ids[0].second;
			errorStr = "The function " + ids[0].first + " already has an implementation";
			return false;
		} else {
			protoDecl->setComplete();
			return true;
		}
	} else {
		std::string typeName, u = "U", text;
		if( kinds.size() > 0 ) {
			typeName = genTypename( kinds[0].first, signs[0] );
		} else {
			typeName = "Int";
		}
			
		text = typeName + std::string(" ( ");
		
		Type* returnType = SymbolTable::getInstance()->findType(typeName);

		//create type representing the function
		FunctionType* func = new FunctionType( returnType, false, false );

		//add arg types
		for( unsigned int i = 1; i < typeclasses.size(); i++ ) {
			std::string argTypeName = genTypename( kinds[i].first, signs[i] );

			Type* arg = SymbolTable::getInstance()->findType(argTypeName);
			func->addArg(arg);
			text = text + argTypeName;
			if( i < typeclasses.size() - 1 ) {
				text = text + std::string(", ");
			} else {
				text = text + std::string(" ");
			}
		}

		text = text + std::string(")");

		//create symbol
		Parser::location_type loc = ids[0].second;
		Symbol* sym = new Symbol( loc, func, text, proto );

		//insert symbol
		if( !SymbolTable::getInstance()->addSymbol(key, sym) ) {
			errorLoc = ids[0].second;
			errorStr = "Function could not be created";
			return false;
		}
		return true;
	}
}

bool Decl::insertTypedef() {
	std::string key = ids[0].first;
	Parser::location_type loc = ids[0].second;
	std::string typeName;

	//generate return type
	typeName = genTypename( kinds[0].first, signs[0] );

	//make the type that the typedef desugars to
	//and then make the typedef itself
	Type *tdef_p = SymbolTable::getInstance()->findType(typeName);
	if( tdef_p == NULL ) {
		errorLoc = kinds[0].second;
		errorStr = "Type not found";
		return false;
	}

	Type *tdef_t = new TypedefType( tdef_p, false, false );
	//make a string for the symbol table to print for the user

	if( SymbolTable::getInstance()->findSymbol(key) ) {
		errorLoc = ids[0].second;
		errorStr = "Typedef shadows existing symbol name";
		return false;
	}

	if( !SymbolTable::getInstance()->addType(key, tdef_t) ) {
		errorLoc = ids[0].second;
		errorStr = "Typedef shadows existing type name";
		return false;
	}
	return true;
}
	


bool Decl::insertBuiltinType( const std::string& key, const Parser::location_type& l ) {
	Sign s = signs[0];
	dtcc::BuiltinType::Kind k = kinds[0].first;
	std::string typeName = genTypename(k, s);

	dtcc::Type *type = dtcc::SymbolTable::getInstance()->findType(typeName);
	dtcc::Symbol *sym = new dtcc::Symbol( l, type, typeName );

	return dtcc::SymbolTable::getInstance()->addSymbol(key, sym);
}

bool Decl::insertUserType( const std::string& typeName, const std::string& key, 
						   const Parser::location_type& l ) {
	//find the type use it to make a symbol
	Type *type = SymbolTable::getInstance()->findType(typeName);
	Symbol *sym = new Symbol( l, type, typeName );
	return SymbolTable::getInstance()->addSymbol(key, sym);
}

bool Decl::insertArray( Type* base, const std::string& key,
			      const Parser::location_type& l, int cap, const std::string& tn ) {
	if( cap < 1 ) {
		errorLoc = l;
		errorStr = "arrays must be atleast size 1";
	}

	std::string text = tn + "[" + std::to_string(cap) + "]";
	Type* type = new ArrayType( base, cap, false, false );
	Symbol *sym = new Symbol( l, type, text );
	return SymbolTable::getInstance()->addSymbol(key, sym);
}



}//namespace
