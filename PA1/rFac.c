#include <stdlib.h>
#include <stdio.h>

int factorial(int n){
    if(n == 1)
        return 1;
    else
        return n * factorial(n-1);
}

int main(int argc, char **argv){
    int n;
    int fac;

    printf("What factorial do you want?\n");
    scanf("%d", &n);

    fac = factorial( n );

    printf("The answer is %d.\n", fac);

    return 0;
}
