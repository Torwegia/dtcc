#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv){
    int n;
    int fac = 1;

    printf("What factorial do you want?\n");
    scanf("%d", &n);

    while(n)
    {
        fac = fac * n;
        n--;
    }

    printf("The answer is %d.\n", fac);

    return 0;
}
