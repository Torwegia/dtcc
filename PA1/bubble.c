#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void swap(char *a, char *b){
    char t;
    t = *a;
    *a = *b;
    *b = t;
    return;
}

void bubble(char *buffer, int size){
    int changed = 1;
    int i;

    while(changed){
        changed = 0;
        for(i = 1; i < size; ++i){
            if( buffer[i-1] > buffer[i]){
                swap( buffer+i-1 , buffer+i );
                changed = 1;
            }
        }
    }
    return;
}


int main(int argc, char **argv){
    int i;
    char *str;
    
    //fill the string
    srand(time(NULL));
    str = (char*) malloc(11);
    str[10] = '\0';

    for(i = 0; i < 10; ++i){
        str[i] = rand() % 26 + 'A';
    }

    printf("pre sorting:  %s\n", str);

    bubble(str, 10);

    printf("post sorting: %s\n", str);


    return 0;
}
