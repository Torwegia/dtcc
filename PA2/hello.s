#Torbjorn Loken
#Hello, World!

.rdata

str: .asciiz "\n\nHello, World!\n"

.text
    
.ent main
main:
    la  $a0, str    #load the address of the string
    li  $v0, 4      #set up the syscall for print string
    syscall

    li  $v0, 10     #set up the syscal for exiting
    syscall
