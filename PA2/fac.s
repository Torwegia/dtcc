#Torbjorn Loken
#Iterative Factorial

.rdata

welcome:    .asciiz "Enter n: "
results:    .asciiz "n! is "
errorStr:   .asciiz "Invalid input\n"
nl:         .asciiz "\n"

.text

.ent main
main:

    la      $a0, nl
    li      $v0, 4
    syscall
    
    #get user input
    la      $a0, welcome
    li      $v0, 4      #print string
    syscall

    li      $v0, 5      #read int
    syscall         #integer now in v0

    bltz    $v0, error

    move    $t0, $v0
    li      $t1, 1

    #calculate the factorial

floop:    
    #eval the base case
    beq     $t0, 0, end

    #mult then subtract
    mul     $t1, $t1, $t0
    sub     $t0, 1

    j       floop
    
error:
    la      $a0, errorStr
    li      $v0, 4
    syscall
    j     exit

end:
    #print result
    la      $a0, results
    li      $v0, 4
    syscall

    move    $a0, $t1
    li      $v0, 1
    syscall

    la      $a0, nl
    li      $v0, 4
    syscall
    
exit:
    #exit
    li  $v0, 10
    syscall
